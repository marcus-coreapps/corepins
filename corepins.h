/*
    *
    * This file is a part of CorePins.
    * A bookmarking app for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#ifndef COREPINS_H
#define COREPINS_H

#include <QWidget>
#include <QListWidgetItem>
#include <QTableWidgetItem>
#include <QDir>
#include <QFileSystemWatcher>

#include "settings.h"

#include <cprime/pinmanage.h>

namespace Ui {
    class corepins;
}

class corepins : public QWidget {
    Q_OBJECT

public:
    explicit corepins(QWidget *parent = nullptr);
    ~corepins();

    void sendFiles(const QStringList &paths);
    void savePin(QString section, QString pinName, QString pinPath, QString pinIcon);
    void reload();

protected:
    void closeEvent(QCloseEvent *cEvent) override;

private slots:
    void openSelectedBookmark(QTableWidgetItem *item);
    void on_section_itemClicked(QListWidgetItem *item);
    void on_addSection_clicked();
    void on_deleteSection_clicked();
    void on_sectionDone_clicked();
    void on_sectionCancel_clicked();
    void on_sectionName_textChanged(const QString &arg1);
    void on_editCancel_clicked();
    void on_editDone_clicked();
    void on_selectSection_currentIndexChanged(const QString &arg1);
    void pinEdit(QTableWidgetItem *item);
    void pinDelete(QTableWidgetItem *item);
    void on_pinName_textChanged(const QString &arg1);
    void on_pinPath_textChanged(const QString &arg1);
    void on_searchPins_textChanged(const QString &arg1);
    void on_btnBrowse_clicked();
    void on_addPin_clicked();
    void on_startAddPin_clicked();
    void showSideView();

private:
    Ui::corepins   *ui;
    settings        *smi;
    bool windowMaximized, bAddPin;
    int             uiMode;
    QSize windowSize, toolsIconSize, listViewIconSize;
    PinManage       pm;
    QDir            currentDir;
    QFileSystemWatcher watcher;
    QString         workFilePath;

    void changeState(bool state);
    void sectionRefresh();
    void startSetup();
    void loadSettings();
    void setupIcons();
};

class pinCellItemWidget : public QWidget
{
    Q_OBJECT

public:
	explicit pinCellItemWidget(QTableWidgetItem *container, settings *smi, QWidget *parent = nullptr);
	~pinCellItemWidget() { }

    pinCellItemWidget *cellItemWidget() {
        return this;
    }

Q_SIGNALS:
    void editOccurred(QTableWidgetItem *item);
    void removeOccurred(QTableWidgetItem *item);

private:
	QTableWidgetItem *m_container;

};

#endif // COREPINS_H
